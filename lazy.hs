import Data.List

divisor n = [x | x <- [1..n], n `mod` x == 0]

quicksort [] = []
quicksort (pivot:xs) =
    let left = quicksort [x | x <- xs, x < pivot]
        right = quicksort [x | x <- xs, x >= pivot]
    in left ++ [pivot] ++ right

permutate [] = [[]]
permutate ls = [ x:ps | x <- ls, ps <- permutate(ls\\[x])]

sieve [] = []
sieve (x:xs) = x : sieve ([z | z <- xs, z `mod` x /= 0])

pythaTriple = [(x, y, z) | z <- [5..], y <- [z-1,z-2.. 1], x <- [y-1,y-2..1], x * x + y * y == z * z]

fibonacci = fibo (1, 1)
    where fibo (x, y) = x : fibo (y, x + y)

factorial = fact [1..]
    where fact (x:y:xs) = x : fact (y*x:xs)