kpk :: Int -> Int -> Int
kpk a b = a * b `div` (gcd a b)

merge [] ys = ys
merge xs [] = xs
merge xa@(x:xs) ya@(y:ys) = if x < y
    then x:merge xs ya
    else y:merge xa ys

mergeSort [] = []
mergeSort [a] = [a]
mergeSort xs =
    let kiri = mergeSort (take (length xs `div` 2) xs)
        kanan = mergeSort (drop (length xs `div` 2) xs)
    in merge kiri kanan
