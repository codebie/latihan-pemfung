import Data.Maybe
data Tree a = EmptyTree | Node a (Tree a) (Tree a) deriving (Show)

foldlTree :: (b -> a -> b) -> b -> Tree a -> b
foldlTree _ acc EmptyTree = acc
foldlTree f acc (Node a left right) = foldlTree f (f (foldlTree f acc left) a) right

data Expr = C Float | Expr :+ Expr | Expr :- Expr
          | Expr :* Expr | Expr :/ Expr 
          | V [Char]
          | Let String Expr Expr      
     deriving Show

subst :: String -> Expr -> Expr -> Expr

subst v0 e0 (V v1)          = if (v0 == v1) then e0 else (V v1)
subst v0 e0 (C c)           = (C c)
subst v0 e0 (e1 :+ e2)      = subst v0 e0 e1 :+ subst v0 e0 e2
subst v0 e0 (e1 :- e2)      = subst v0 e0 e1 :- subst v0 e0 e2
subst v0 e0 (e1 :* e2)      = subst v0 e0 e1 :* subst v0 e0 e2
subst v0 e0 (e1 :/ e2)      = subst v0 e0 e1 :/ subst v0 e0 e2
subst v0 e0 (Let v1 e1 e2)  = Let v1 e1 (subst v0 e0 e2)

evaluate :: Expr -> Float
evaluate (C x)          = x
evaluate (e1 :* e2)     = evaluate e1 * evaluate e2
evaluate (e1 :/ e2)     = evaluate e1 / evaluate e2
evaluate (e1 :+ e2)     = evaluate e1 + evaluate e2
evaluate (e1 :- e2)     = evaluate e1 - evaluate e2
evaluate (Let v e0 e1)  = evaluate (subst v e0 e1)
evaluate (V v)          = 0.0

-- evaluate dengan fold
foldlExpr f acc (C con)          = f acc con
foldlExpr f acc (V v)            = f acc 0.0
foldlExpr f acc (Let v e0 e1)    = foldlExpr f acc (subst v e0 e1)
foldlExpr f acc (e1 :* e2)       = foldlExpr (*) (foldlExpr f acc e1) e2
foldlExpr f acc (e1 :/ e2)       = foldlExpr (/) (foldlExpr f acc e1) e2
foldlExpr f acc (e1 :+ e2)       = foldlExpr (+) (foldlExpr f acc e1) e2
foldlExpr f acc (e1 :- e2)       = foldlExpr (-) (foldlExpr f acc e1) e2

evaluate' :: Expr -> Float
evaluate' = foldlExpr (+) 0

-- parameterize fold
foldExpr' (c, v, l, times, div, plus, minus) (C con)           = c con
foldExpr' (c, v, l, times, div, plus, minus) (V var)           = v var
foldExpr' (c, v, l, times, div, plus, minus) (Let var e0 e1)   = foldExpr' (c, v, l, times, div, plus, minus) (l var e0 e1)
foldExpr' (c, v, l, times, div, plus, minus) (e1 :* e2)        = times (foldExpr' (c, v, l, times, div, plus, minus) e1) (foldExpr' (c, v, l, times, div, plus, minus) e2)
foldExpr' (c, v, l, times, div, plus, minus) (e1 :/ e2)        = div (foldExpr' (c, v, l, times, div, plus, minus) e1) (foldExpr' (c, v, l, times, div, plus, minus) e2)
foldExpr' (c, v, l, times, div, plus, minus) (e1 :+ e2)        = plus (foldExpr' (c, v, l, times, div, plus, minus) e1) (foldExpr' (c, v, l, times, div, plus, minus) e2)
foldExpr' (c, v, l, times, div, plus, minus) (e1 :- e2)        = minus (foldExpr' (c, v, l, times, div, plus, minus) e1) (foldExpr' (c, v, l, times, div, plus, minus) e2)

evaluate1 :: Expr -> Float
evaluate1 = foldExpr' (c, v, l, times, div, plus, minus)
    where
        c = id
        v = (\x -> 0.0)
        l = subst
        times = (*)
        div = (/)
        plus = (+)
        minus = (-)

countCons = foldExpr' (c, v, l, times, div, plus, minus)
    where
        c = (\x -> 1)
        v = (\x -> 0)
        l _ _ = id
        times = (+)
        div = (+)
        plus = (+)
        minus = (+)

countOp = foldExpr' (c, v, l, times, div, plus, minus)
    where
        c = (\x -> 0)
        v = (\x -> 0)
        l _ _ = id
        times = (+) . (+1)
        div = (+) . (+1)
        plus = (+) . (+1)
        minus = (+) . (+1)

countVar = foldExpr' (c, v, l, times, div, plus, minus)
    where
        c = (\x -> 0)
        v = (\x -> 1)
        l _ _ = id
        times = (+)
        div = (+)
        plus = (+)
        minus = (+)

divByZero expr = isNothing (foldExpr' (c, v, l, times, div, plus, minus) expr)
    where
        c = (\x -> Just x)
        v = (\x -> Just 0.0)
        l = subst
        times (Just x) (Just y) = Just (x * y)
        times _ _ = Nothing
        div (Just x) (Just y) = if y == 0.0 then Nothing else (Just (x / y))
        div _ _ = Nothing
        plus (Just x) (Just y) = Just (x + y)
        plus _ _ = Nothing
        minus (Just x) (Just y) = Just (x - y)
        minus _ _ = Nothing