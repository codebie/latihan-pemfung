import qualified Data.Maybe as Maybe

-- length using sum and map
length' = sum . map (\x -> 1)

-- iter type and definition
iter :: Int -> (a -> a) -> a -> a
iter 0 _ x = x
iter n f x = f (iter (n-1) f x)

-- sum of squares 1 to n using map, foldr
sumSquares n = foldr (+) 0 (map (\x -> x * x) [1..n])

-- composeList
composeList (x:[]) = (\y -> x y)
composeList (x:xs) = (\y -> x (composeList xs y))

-- flip
flip' :: (a -> b -> c) -> (b -> a -> c)
flip' f = (\x y -> f y x)

-- [ x+1 | x <-xs ]
lc1 = map (+1)
-- [ x+y | x <-xs, y <-ys ]
lc2 = (\xs ys -> map (\(x,y) -> x+y) (zip xs ys))
-- [ x+2 | x <-xs, x > 3 ]
lc3 = map (+2) . filter (>3)
-- [ x+3 | (x,_) <-xys ]
lc4 = map (\(x,_) -> x+3)
-- [ x+4 | (x,y) <-xys, x+y < 5 ]
lc5 = map (\(x,_) -> x+4) . filter (\(x,y) -> x + y < 5)
-- [ x+5 | Just x <-mxs ]
lc6 = map (\(Just x) -> x + 5) . filter Maybe.isJust

-- map (+3) xs
ilc1 xs = [ x + 3 | x <- xs]
-- filter (>7) xs
ilc2 xs = [ x | x <- xs, x > 7]
-- concat (map (\x -> map (\y -> (x,y)) ys) xs)
ilc3 xs ys = [(x, y) | x <- xs, y <- ys]
-- filter (>3) (map (\(x,y) -> x+y) xys)
ilc4 xys = [x + y | (x, y) <- xys, x + y > 3]